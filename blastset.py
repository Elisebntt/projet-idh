#!/usr/bin/env python
# Copyright 2018 BARRIOT Roland
# This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import numpy as np
from os.path import isfile
from scipy.stats import binom, hypergeom, chi2_contingency

# SCRIPT PARAMETERS
# e.g. ./blastset.py --sets Canis_lupus_directs.sets --query 'ENSCAFP00000020480 ENSCAFP00000026101 ENSCAFP00000019615 ENSCAFP00000037722 ENSCAFP00000023027 ENSCAFP00000038219 ENSCAFP00000043222 ENSCAFP00000016302' --adjust --alpha 0.05 --measure 
parser = argparse.ArgumentParser(description='Search enriched terms/categories in the provided (gene) set')
parser.add_argument('-q', '--query', required=True, help='Query set.')
parser.add_argument('-t', '--sets', required=True, help='Target sets (categories).')
parser.add_argument('-a', '--alpha', required=False, type=float, default=0.05, help='Significance threshold.')
parser.add_argument('-c', '--adjust', required=False, action="store_true", help='Adjust for multiple testing (FDR).')
parser.add_argument('-m', '--measure', required=False, default='binomial', help='Dissimilarity index: binomial (default), hypergeometric, chi2 or coverage.')
parser.add_argument('-l', '--limit', required=False, type=int, default=0, help='Maximum number of results to report.')
param = parser.parse_args()

class ComparedSet(object):
	def __init__(self, id, name = '', common = 0, size = 0, pvalue = 1, elements = [], common_elements = []):
		self.id = id
		self.name = name
		self.common = common
		self.size = size
		self.pvalue = pvalue
		self.elements = elements
		self.common_elements = common_elements

# LOAD QUERY
text = param.query
query = set() # type d'objet de python permettant des outils supplémentaires par rapport à la liste
if isfile(text): # S'il s'agit d'un fichier on traite ligne par ligne
	with open(text) as f:
		content = f.read()
		lines = content.split('\n')
		for l in lines:
			if l!='':
				query |= set(l.split()) # découpe chaque ligne en mots et les incorpore dans la liste de requête
else: # parse string si c'est juste une chaine de caractère
	query |= set(text.split())

# LOAD REFERENCE SETS
def load_sets(filename):
	sets = {} # définition du voisinage par la création d'un dictionnaire  
	ids = set()# pour le prochain ensemble cible que l'on chargera
	with open( filename ) as f: 
		content = f.read()
		lines = content.split('\n')
		for l in lines:
			words = l.split('\t') # Découpe par le nombre d'identifiant sur la ligne
			if len(words) > 2 and not words[0].startswith('#'): # On ignore les lignes commencant par une #, donc les lignes de commentaires 
				id = words.pop(0)
				name = words.pop(0)
				words = set(words)
				sets[ id ] = { 'name': name, 'elements': words} # pointe vers un dictionnaire : nom du pathway (clé du dictionnaire) et la liste d'éléments le composant
				ids |= words
	return [ sets, len( ids ) ] # len(ids) permet d'avoir la taille de population
(sets, population_size) = load_sets(param.sets)

# EVALUATE SETS
results = [] # Liste triée du plus au moins similaire
query_size = len(query)
for id in sets: # pour toute les clés du dico sets
	elements = sets[ id ][ 'elements' ]
	common_elements = elements.intersection( query ) # Calcul des éléments commun entre Query et Targuet
	if param.measure=='binomial': # binom.cdf(>=success, attempts, proba)
		# p_success = 384/2064, 152 attempts, 61 success
		#~ pval = binom.pmf(61, 152, 384.0/2064)
		pval = binom.cdf( query_size - len(common_elements), query_size, 1 - float(len(elements))/population_size)
	elif param.measure=='hypergeometric': # hypergeom.sf(common-1, population, target, query) = 1-p( X <= x-1 ) = p( X >= x )
		pval = hypergeom.sf(len(common_elements)-1, population_size, len(elements), query_size)
	elif param.measure=='chi2':
		tab_contingence= np.array([[len(common_elements), (query_size - len(common_elements))],
								[(len(elements) - len(common_elements)), (population_size - query_size - len(elements) + len(common_elements))]])
		if (len(common_elements)!=0): #si il y a des éléments en communs
			pval = chi2_contingency(tab_contingence)[1] 
		else:
			break  
	elif param.measure=='coverage': # Méthode de dissemblance naïve/non statistique
		pval = 1 - (len(common_elements) / query_size) * (len(common_elements) / len(elements))
	else:
		print('sorry, %s not (yet) implemented' % ( param.measure ))
		exit(1)
	r = ComparedSet( id, sets[id]['name'], len(common_elements), len(elements), pval, elements, common_elements)
	results.append( r ) # Chaque ligne est ajoutée à la sortie résultat

# IMPRIME LES RÉSULTATS SIGNIFICATIFS
results.sort(key=lambda an_item: an_item.pvalue) # Tri des résultats(de type ComparedSet) par pvalue croissante
i=1
for r in results:
	if param.measure=='coverage' and r.pvalue==1 : break #si la mesure choisit est la 'coverage' alors on ne print pas les résultats avec une 'pvalue' de 1, c'est-à-dire les dissemblants	
	# FDR
	if param.adjust and r.pvalue > param.alpha * i / len(results): break # si pvalue > seuil ajusté on break la boucle d'impression des résultats car les suivant ne sont pas significatifs
	# limited output
	if param.limit > 0 and i>param.limit: break
	# alpha threshold
	elif r.pvalue > param.alpha: break # si pvalue plus significative on arrête la boucle d'impression des résultats 
	# OUTPUT
	print("%s\t%s\t%s/%s\t%s\t%s" % ( r.id, r.pvalue, r.common, r.size, r.name, ', '.join(r.common_elements)))
	i+=1
