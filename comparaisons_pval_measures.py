#!/usr/bin/env python
# Comparaison des mesures de dissemblance selon différentes cardinalité des ensembles 
import numpy as np
from scipy.stats import binom, hypergeom, chi2_contingency
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

# Fonction permettant de calculer la p-value en fonction de la mesure entrée et de la cardinalité de Q, T et C, G sera fixé
def Calcul_pval(measure, Q, T, C):
	G = 100 #G a été fixé à 100
	if measure == 'binomial' : 
		pval = binom.cdf( Q - C, Q, 1 - T/G)
	elif measure == 'hypergeometric' :
		pval = hypergeom.sf(C - 1, G, T, Q)
	elif measure == 'chi2' :
		contingence = np.array([[C, (Q - C)],
								[(T - C), (G - Q - T) + C]])
		pval = chi2_contingency(contingence)[1]
	elif measure == 'coverage' : 
		pval = 1 - (C / Q) * (C / T)
	return pval



# Fonction pour la création de vecteurs comprenant les différentes valeurs de cardinalités de Q, T et C ainsi que la p-value associée
def Creation_Vecteurs(measure) : 
	vecteur_Q = []
	vecteur_T = []
	vecteur_C = []
	vecteur_pval = []
	for Q in range(1, 30) :
		for T in range (1, 30) :
			for C in range (1, min(Q, T)) :
				vecteur_Q.append(Q)
				vecteur_T.append(T)
				vecteur_C.append(C)
				vecteur_pval.append(Calcul_pval(measure, Q, T, C))
	return (vecteur_Q, vecteur_T, vecteur_C, vecteur_pval)

Q_binomial, T_binomial, C_binomial, pval_binomial = Creation_Vecteurs('binomial')
Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric = Creation_Vecteurs('hypergeometric')
Q_chi2, T_chi2, C_chi2, pval_chi2 = Creation_Vecteurs('chi2')
Q_coverage, T_coverage, C_coverage, pval_coverage = Creation_Vecteurs('coverage')

# Fonction permettant la visualisation
def Visualisation(vecteur_Q, vecteur_T, vecteur_C, vecteur_pval): 
	fig = plt.figure()
	ax = fig.gca(projection='3d')
	surface = ax.scatter(vecteur_Q, vecteur_T, vecteur_C, zdir='z', s = 30, c = vecteur_pval, depthshade=True)
	ax.set_xlabel('Q')
	ax.set_ylabel('T')
	ax.set_zlabel('C');
	fig.colorbar(surface)
	title = ax.set_title("P-value en fonction de différentes tailles de Q, T and C")
	title.set_y(1.01)
	plt.show()

Visualisation(Q_binomial, T_binomial, C_binomial, pval_binomial)
Visualisation(Q_hypergeometric, T_hypergeometric, C_hypergeometric, pval_hypergeometric)
Visualisation(Q_chi2, T_chi2, C_chi2, pval_chi2)
Visualisation(Q_coverage, T_coverage, C_coverage, pval_coverage)

